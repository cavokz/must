use std::thread;
use std::sync::mpsc;
use std::fmt::{Error, Write};

use must::*;

fn format_scales(note: Note) -> Result<String, Error> {
    let mut s = String::new();

    write!(s, "{} major:", note)?;
    for note in note.major_scale().take(8) {
        write!(s, " {}", note)?;
    }
    writeln!(s)?;

    write!(s, "{} minor:", note)?;
    for note in note.minor_scale().take(8) {
        write!(s, " {}", note)?;
    }
    writeln!(s)?;

    write!(s, "{} melodic:", note)?;
    for note in note.melodic_scale().take(8) {
        write!(s, " {}", note)?;
    }

    Ok(s)
}

fn main() {
    let (tx, rx) = mpsc::channel();

    for note in [Note::A, Note::B, Note::C, Note::D, Note::E, Note::F, Note::G].iter() {
        for accident in [None, Some(Accident::Sharp), Some(Accident::Flat)].iter() {
            let t = mpsc::Sender::clone(&tx);
            thread::spawn(move || {
                t.send(format_scales(note(*accident))).unwrap();
            });
        }
    }

    drop(tx);
    for r in rx {
        println!("{}", r.unwrap());
    }
}
