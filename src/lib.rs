#[warn(unused_variables)]

use std::clone;
use std::fmt;

#[derive(Copy, PartialEq, Debug)]
pub enum Accident {
    Sharp,
    Flat,
    DoubleSharp,
    DoubleFlat,
}

impl clone::Clone for Accident {
    fn clone(&self) -> Accident {
        *self
    }
}

impl fmt::Display for Accident {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Accident::Sharp => write!(f, "#"),
            Accident::Flat => write!(f, "b"),
            Accident::DoubleSharp => write!(f, "##"),
            Accident::DoubleFlat => write!(f, "bb"),
        }
    }
}

#[derive(Copy, PartialEq, Debug)]
pub enum Note {
    A(Option<Accident>),
    B(Option<Accident>),
    C(Option<Accident>),
    D(Option<Accident>),
    E(Option<Accident>),
    F(Option<Accident>),
    G(Option<Accident>),
}

impl Note {
    fn next_tone(&self) -> Note {
        match self {
            Note::B(None) => Note::C(Some(Accident::Sharp)),
            Note::E(None) => Note::F(Some(Accident::Sharp)),
            Note::B(Some(Accident::Sharp)) => Note::C(Some(Accident::DoubleSharp)),
            Note::E(Some(Accident::Sharp)) => Note::F(Some(Accident::DoubleSharp)),
            Note::B(Some(Accident::Flat)) => Note::C(None),
            Note::E(Some(Accident::Flat)) => Note::F(None),
            Note::B(Some(Accident::DoubleFlat)) => Note::C(Some(Accident::Flat)),
            Note::E(Some(Accident::DoubleFlat)) => Note::F(Some(Accident::Flat)),
            Note::A(accident) => Note::B(*accident),
            Note::C(accident) => Note::D(*accident),
            Note::D(accident) => Note::E(*accident),
            Note::F(accident) => Note::G(*accident),
            Note::G(accident) => Note::A(*accident),
            _ => panic!("{} is not supported", self),
        }
    }

    fn next_semitone(&self) -> Note {
        match self {
            Note::A(None) => Note::B(Some(Accident::Flat)),
            Note::C(None) => Note::D(Some(Accident::Flat)),
            Note::D(None) => Note::E(Some(Accident::Flat)),
            Note::F(None) => Note::G(Some(Accident::Flat)),
            Note::G(None) => Note::A(Some(Accident::Flat)),
            Note::A(Some(Accident::Sharp)) => Note::B(None),
            Note::C(Some(Accident::Sharp)) => Note::D(None),
            Note::D(Some(Accident::Sharp)) => Note::E(None),
            Note::F(Some(Accident::Sharp)) => Note::G(None),
            Note::G(Some(Accident::Sharp)) => Note::A(None),
            Note::A(Some(Accident::DoubleSharp)) => Note::B(Some(Accident::Sharp)),
            Note::C(Some(Accident::DoubleSharp)) => Note::D(Some(Accident::Sharp)),
            Note::D(Some(Accident::DoubleSharp)) => Note::E(Some(Accident::Sharp)),
            Note::F(Some(Accident::DoubleSharp)) => Note::G(Some(Accident::Sharp)),
            Note::G(Some(Accident::DoubleSharp)) => Note::A(Some(Accident::Sharp)),
            Note::A(Some(Accident::Flat)) => Note::B(Some(Accident::DoubleFlat)),
            Note::C(Some(Accident::Flat)) => Note::D(Some(Accident::DoubleFlat)),
            Note::D(Some(Accident::Flat)) => Note::E(Some(Accident::DoubleFlat)),
            Note::F(Some(Accident::Flat)) => Note::G(Some(Accident::DoubleFlat)),
            Note::G(Some(Accident::Flat)) => Note::A(Some(Accident::DoubleFlat)),
            Note::B(accident) => Note::C(*accident),
            Note::E(accident) => Note::F(*accident),
            _ => panic!("{} is not supported", self),
        }
    }

    pub fn major_scale(&self) -> Scale {
        Scale::new(self, "TTsTTTs")
    }

    pub fn minor_scale(&self) -> Scale {
        Scale::new(self, "TsTTsTT")
    }

    pub fn melodic_scale(&self) -> Scale {
        Scale::new(self, "TsTTTTs")
    }
}

impl clone::Clone for Note {
    fn clone(&self) -> Note {
        *self
    }
}

impl fmt::Display for Note {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Note::A(None) => write!(f, "A"),
            Note::B(None) => write!(f, "B"),
            Note::C(None) => write!(f, "C"),
            Note::D(None) => write!(f, "D"),
            Note::E(None) => write!(f, "E"),
            Note::F(None) => write!(f, "F"),
            Note::G(None) => write!(f, "G"),
            Note::A(Some(accident)) => write!(f, "{}A", accident),
            Note::B(Some(accident)) => write!(f, "{}B", accident),
            Note::C(Some(accident)) => write!(f, "{}C", accident),
            Note::D(Some(accident)) => write!(f, "{}D", accident),
            Note::E(Some(accident)) => write!(f, "{}E", accident),
            Note::F(Some(accident)) => write!(f, "{}F", accident),
            Note::G(Some(accident)) => write!(f, "{}G", accident),
        }
    }
}

pub struct Scale {
    mode: String,
    note: Note,
    pos: usize,
}

impl Scale {
    pub fn new(note: &Note, mode: &str) -> Scale {
        for c in mode.chars() {
            match c {
                'T' => (),
                's' => (),
                 _  => panic!("invalid mode '{}'", mode),
            }
        }
        Scale {
            mode: String::from(mode),
            note: *note,
            pos: 0,
        }
    }
}

impl Iterator for Scale {
    type Item = Note;

    fn next(&mut self) -> Option<Note> {
        let ret = Some(self.note);
        self.note = match self.mode.chars().nth(self.pos) {
            Some('T') => self.note.next_tone(),
            Some('s') => self.note.next_semitone(),
            _ => panic!("invalid mode at position {} of {}", self.pos, self.mode),
        };
        self.pos = (self.pos + 1) % self.mode.len();
        ret
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn A_major_scale() {
        let mut scale = Note::A(None).major_scale();
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(None));
    }

    #[test]
    fn A_minor_scale() {
        let mut scale = Note::A(None).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
    }

    #[test]
    fn A_melodic_scale() {
        let mut scale = Note::A(None).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(None));
    }

    #[test]
    fn Asharp_major_scale() {
        let mut scale = Note::A(Some(Accident::Sharp)).major_scale();
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
    }

    #[test]
    fn Asharp_minor_scale() {
        let mut scale = Note::A(Some(Accident::Sharp)).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
    }

    #[test]
    fn Asharp_melodic_scale() {
        let mut scale = Note::A(Some(Accident::Sharp)).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
    }

    #[test]
    fn Aflat_major_scale() {
        let mut scale = Note::A(Some(Accident::Flat)).major_scale();
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
    }

    #[test]
    fn Aflat_minor_scale() {
        let mut scale = Note::A(Some(Accident::Flat)).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
    }

    #[test]
    fn Aflat_melodic_scale() {
        let mut scale = Note::A(Some(Accident::Flat)).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
    }

    #[test]
    fn B_major_scale() {
        let mut scale = Note::B(None).major_scale();
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(None));
    }

    #[test]
    fn B_minor_scale() {
        let mut scale = Note::B(None).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
    }

    #[test]
    fn B_melodic_scale() {
        let mut scale = Note::B(None).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(None));
    }

    #[test]
    fn Bsharp_major_scale() {
        let mut scale = Note::B(Some(Accident::Sharp)).major_scale();
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
    }

    #[test]
    fn Bsharp_minor_scale() {
        let mut scale = Note::B(Some(Accident::Sharp)).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
    }

    #[test]
    fn Bsharp_melodic_scale() {
        let mut scale = Note::B(Some(Accident::Sharp)).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
    }

    #[test]
    fn Bflat_major_scale() {
        let mut scale = Note::B(Some(Accident::Flat)).major_scale();
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
    }

    #[test]
    fn Bflat_minor_scale() {
        let mut scale = Note::B(Some(Accident::Flat)).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
    }

    #[test]
    fn Bflat_melodic_scale() {
        let mut scale = Note::B(Some(Accident::Flat)).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
    }

    #[test]
    fn C_major_scale() {
        let mut scale = Note::C(None).major_scale();
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(None));
    }

    #[test]
    fn C_minor_scale() {
        let mut scale = Note::C(None).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
    }

    #[test]
    fn C_melodic_scale() {
        let mut scale = Note::C(None).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(None));
    }

    #[test]
    fn Csharp_major_scale() {
        let mut scale = Note::C(Some(Accident::Sharp)).major_scale();
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
    }

    #[test]
    fn Csharp_minor_scale() {
        let mut scale = Note::C(Some(Accident::Sharp)).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
    }

    #[test]
    fn Csharp_melodic_scale() {
        let mut scale = Note::C(Some(Accident::Sharp)).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
    }

    #[test]
    fn Cflat_major_scale() {
        let mut scale = Note::C(Some(Accident::Flat)).major_scale();
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
    }

    #[test]
    fn Cflat_minor_scale() {
        let mut scale = Note::C(Some(Accident::Flat)).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
    }

    #[test]
    fn Cflat_melodic_scale() {
        let mut scale = Note::C(Some(Accident::Flat)).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
    }

    #[test]
    fn D_major_scale() {
        let mut scale = Note::D(None).major_scale();
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(None));
    }

    #[test]
    fn D_minor_scale() {
        let mut scale = Note::D(None).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
    }

    #[test]
    fn D_melodic_scale() {
        let mut scale = Note::D(None).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(None));
    }

    #[test]
    fn Dsharp_major_scale() {
        let mut scale = Note::D(Some(Accident::Sharp)).major_scale();
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
    }

    #[test]
    fn Dsharp_minor_scale() {
        let mut scale = Note::D(Some(Accident::Sharp)).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
    }

    #[test]
    fn Dsharp_melodic_scale() {
        let mut scale = Note::D(Some(Accident::Sharp)).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
    }

    #[test]
    fn Dflat_major_scale() {
        let mut scale = Note::D(Some(Accident::Flat)).major_scale();
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
    }

    #[test]
    fn Dflat_minor_scale() {
        let mut scale = Note::D(Some(Accident::Flat)).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
    }

    #[test]
    fn Dflat_melodic_scale() {
        let mut scale = Note::D(Some(Accident::Flat)).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
    }

    #[test]
    fn E_major_scale() {
        let mut scale = Note::E(None).major_scale();
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(None));
    }

    #[test]
    fn E_minor_scale() {
        let mut scale = Note::E(None).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
    }

    #[test]
    fn E_melodic_scale() {
        let mut scale = Note::E(None).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(None));
    }

    #[test]
    fn Esharp_major_scale() {
        let mut scale = Note::E(Some(Accident::Sharp)).major_scale();
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
    }

    #[test]
    fn Esharp_minor_scale() {
        let mut scale = Note::E(Some(Accident::Sharp)).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
    }

    #[test]
    fn Esharp_melodic_scale() {
        let mut scale = Note::E(Some(Accident::Sharp)).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
    }

    #[test]
    fn Eflat_major_scale() {
        let mut scale = Note::E(Some(Accident::Flat)).major_scale();
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
    }

    #[test]
    fn Eflat_minor_scale() {
        let mut scale = Note::E(Some(Accident::Flat)).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
    }

    #[test]
    fn Eflat_melodic_scale() {
        let mut scale = Note::E(Some(Accident::Flat)).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
    }

    #[test]
    fn F_major_scale() {
        let mut scale = Note::F(None).major_scale();
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(None));
    }

    #[test]
    fn F_minor_scale() {
        let mut scale = Note::F(None).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
    }

    #[test]
    fn F_melodic_scale() {
        let mut scale = Note::F(None).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(None));
    }

    #[test]
    fn Fsharp_major_scale() {
        let mut scale = Note::F(Some(Accident::Sharp)).major_scale();
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
    }

    #[test]
    fn Fsharp_minor_scale() {
        let mut scale = Note::F(Some(Accident::Sharp)).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
    }

    #[test]
    fn Fsharp_melodic_scale() {
        let mut scale = Note::F(Some(Accident::Sharp)).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
    }

    #[test]
    fn Fflat_major_scale() {
        let mut scale = Note::F(Some(Accident::Flat)).major_scale();
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Flat)));
    }

    #[test]
    fn Fflat_minor_scale() {
        let mut scale = Note::F(Some(Accident::Flat)).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Flat)));
    }

    #[test]
    fn Fflat_melodic_scale() {
        let mut scale = Note::F(Some(Accident::Flat)).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Flat)));
    }

    #[test]
    fn G_major_scale() {
        let mut scale = Note::G(None).major_scale();
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(None));
    }

    #[test]
    fn G_minor_scale() {
        let mut scale = Note::G(None).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(None));
    }

    #[test]
    fn G_melodic_scale() {
        let mut scale = Note::G(None).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::G(None));
        assert_eq!(scale.next().unwrap(), Note::A(None));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(None));
        assert_eq!(scale.next().unwrap(), Note::D(None));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(None));
    }

    #[test]
    fn Gsharp_major_scale() {
        let mut scale = Note::G(Some(Accident::Sharp)).major_scale();
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
    }

    #[test]
    fn Gsharp_minor_scale() {
        let mut scale = Note::G(Some(Accident::Sharp)).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(None));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
    }

    #[test]
    fn Gsharp_melodic_scale() {
        let mut scale = Note::G(Some(Accident::Sharp)).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::B(None));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Sharp)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::DoubleSharp)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Sharp)));
    }

    #[test]
    fn Gflat_major_scale() {
        let mut scale = Note::G(Some(Accident::Flat)).major_scale();
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
    }

    #[test]
    fn Gflat_minor_scale() {
        let mut scale = Note::G(Some(Accident::Flat)).minor_scale();
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::F(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
    }

    #[test]
    fn Gflat_melodic_scale() {
        let mut scale = Note::G(Some(Accident::Flat)).melodic_scale();
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::A(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::B(Some(Accident::DoubleFlat)));
        assert_eq!(scale.next().unwrap(), Note::C(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::D(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::E(Some(Accident::Flat)));
        assert_eq!(scale.next().unwrap(), Note::F(None));
        assert_eq!(scale.next().unwrap(), Note::G(Some(Accident::Flat)));
    }
}
